(function() {
    function agentInfo(AgentSvc) {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'agent-info/agent-info.html',
            link: function(scope) {
                scope.agent = null;

                scope.$watchCollection(AgentSvc.currentAgent, function(agent) {
                    scope.agent = agent;
                });
            }
        };
    }

    angular
        .module('phonePanelApp.agentInfo', ['liveops'])
        .directive('agentInfo', agentInfo);
})();