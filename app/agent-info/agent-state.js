(function() {
    function agentState() {
        return {
            restrict: 'E',
            replace: true,
            template: [
                '<select class="state"',
                'ng-options="state for state in states"',
                'ng-model="selectedState"',
                'ng-change="selectState()"></select>'
            ].join(" "),

            link: function(scope) {
                scope.states = [
                    "OFFLINE",
                    "ONLINE"
                ];

                scope.selectedState = "OFFLINE";

                scope.selectState = function() {

                };
            }
        };
    }

    angular
        .module('phonePanelApp.agentInfo')
        .directive('agentState', agentState);
})();