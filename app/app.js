(function() {
    function config($routeProvider) {
        $routeProvider.when("/offline",{
            templateUrl: "templates/offline.html"
        }).when("/online",{
            templateUrl: "templates/online.html"
        }).otherwise({redirectTo: "/offline"});
    }

    function run() {

    }

    angular
        .module('phonePanelApp', ['ngRoute', 'ngCookies', 'liveops', 'phonePanelApp.topBar', 'phonePanelApp.agentInfo', 'phonePanelApp.workList'])
        .config(config)
        .run(run);
})();
