(function() {
    function workList() {
        return {
            restrict: 'E',
            replace: true,
            template: [
                '<select class="workList"></select>'
            ].join(""),
            link: function(scope) {

            }
        };
    }

    angular
        .module('phonePanelApp.workList', [])
        .directive('workList', workList);
})();