(function() {
    function controller($scope, $cookieStore, $location, $route, AgentSvc) {
        var currentAgent = null;

        $scope.$watchCollection(AgentSvc.currentAgent, function(agent) {
            currentAgent = agent;

            if (typeof agent === "undefined" || agent === null) {
                $location.path("offline");
            } else {
                $location.path("online");
            }
        });

        function agentExists() {
            return currentAgent != null;
        }

        $scope.agentExists = function() {
            return agentExists();
        };

        $scope.agentIsOnline = function() {
            return (currentAgent && currentAgent.isOnline);
        };

        $scope.login = {};

        $scope.doLogin = function() {
            var login = $scope.login;

            AgentSvc.initializeWithUsernameAndPassword(login.username, login.password, null, function() {
                var agent = AgentSvc.currentAgent();    
                agent.goOnline({typeConstant: 11});
            });
        }

    }

    angular
        .module('phonePanelApp')
        .controller('MainCtrl', controller);
})();
