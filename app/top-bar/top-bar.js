(function() {
    function topBar() {
        return {
            restrict: 'E',
            replace: true,
            template: [
                '<div class="topBar">',
                '<img src="../images/bar-logo.png" />',
                '</div>'
            ].join("")
        };
    }

    angular
        .module('phonePanelApp.topBar', [])
        .directive('topBar', topBar);
})();