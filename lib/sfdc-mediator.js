/**
 * Created by srobinson on 17/09/14.
 */
(function (LIVEOPS, sforce, $) {

    var sfdcMediator = {},
        currentSFDCObject = null;

    function log(message){
        console.log("sfdcMediator " + message);
    }

    function isWhoObjectType(sfdcObject) {

        var objectType = sfdcObject['object'];
        if (['Contact', 'Lead'].indexOf(objectType) > -1) {
            return true;
        }
        return false;
    }

    function getSearchParams(call) {
        log("getSearchParams ...");
        var phoneNumber = call.ani;

        var phonNumberWithoutSymbols = phoneNumber.replace(/\D/g,"");
        var phoneNumberWithoutLeadOne = phonNumberWithoutSymbols.replace(/^1/,"");

        var searchValues = [];

        searchValues.push(phonNumberWithoutSymbols);
        searchValues.push(phoneNumberWithoutLeadOne);

        var numberFormats = LIVEOPS.xb.xlatePhone.getNumberFormats(phoneNumber);
        searchValues = searchValues.concat(numberFormats);

        var searchString = "";

        var length = searchValues.length;
        for (var i=0; i<length; i++) {
            if (i > 0 ) {
                searchString += " OR ";
            }
            searchString += searchValues[i];
        }

        searchParams = {
            searchType: 'searchAndScreenPop',
            searchString: searchString
        }

        log("getSearchParams " + searchParams);
        return searchParams;
    }

    function handleClickToDial(enable) {

        log("handleClickToDial enable " + enable);
        var strLogText = enable ? "enabled" : "disabled";

        var callback = function (response) {
            if (response.result) {
                log('Click to dial was ' + strLogText);
            } else {
                log('Click to dial was NOT ' + strLogText + '. Error: ' +
                    response.error);
            }
        };

        if(enable){
            sforce.interaction.cti.enableClickToDial(callback);
        } else {
            sforce.interaction.cti.disableClickToDial(callback);
        }

    }

    function saveSfdcTask(sfdcObject) {

        log("saveSfdcTask sfdcObject " + sfdcObject);
        var data = {};

        if (sfdcObject) {
            if (isWhoObjectType(sfdcObject)) {
                data.whoId = sfdcObject.objectId;
            } else { //releated to
                data.whatId = sfdcObject.objectId;
            }
        }

        for(var o in sfdcObject){
            this.data[o] = sfdcObject[o];
        }

        var taskData = data ? $.param(data) : {};

        sforce.interaction.saveLog('Task', taskData, function(e){
            // Set the task status message if current call was saved
            if (e.error) {
                log("Issue saving task " + response.error);
            } else {
                currentSFDCObject.Id = response.result;
            }
        });

    }

    function onSearchCallBack(e) {
        var results = JSON.parse(e.result);

        // Object to array
        var resultsArray = new Array();
        for (var obj in results) {
            results[obj].objectId = obj;
            results[obj].objectName = results[obj].Name;
            resultsArray.push(results[obj]);
        }

        if (resultsArray.length === 1) {
            saveSfdcTask(resultsArray[0]);
        }
    }

    function onCallStart(e) {
        log("onCallStart called");

        var callType = null;

        if (currentSFDCObject) {
            callType = 'outbound';
            saveSfdcTask(e.call, currentSFDCObject);
        } else {
            callType = 'inbound';

            var searchParams = getSearchParams(e.call);
            var searchString = searchParams.searchString;

            sforce.interaction.searchAndScreenPop(
                searchString,
                null,
                callType,
                function(e){
                    onSearchCallBack(e);
                });
        }
    }

    function onCallEnd(e) {
        log("onCallEnd called");


        currentSFDCObject.CallDurationInSeconds = parseInt( (new Date().getTime() -
            e.call.clientCallStartTime) / LIVEOPS.xb.ONE_SECOND, 10);

        currentSFDCObject = null;
    }

    function onAgentOnline(e) {
        log("onAgentOnline called");
        handleClickToDial(true);
    }

    function onAgentOffline(e) {
        log("onAgentOffline called");
        handleClickToDial(false);
    }

    function onClickToDial(e){
        log("onClickToDial called");

        var sfdcObject = JSON.parse(e.result),
            cleanPhoneNumber =
                LIVEOPS.xb.xlatePhone.uiToFunctional(sfdcObject.number),
            campaignId = 0,
            campaignText = "";

        currentSFDCObject = sfdcObject;
        var ws = LIVEOPS.desktopAPI.agent.workSession;
        if(ws){
            log("Dialing number: " + cleanPhoneNumber + " campaign : "+  campaignText);
            var campaign = new LIVEOPS.desktopAPI.Campaign(
                parseInt(campaignId), campaignText);
            ws.dialNumber(cleanPhoneNumber, campaign);
        }

    }

    function registerEventListener() {
        LIVEOPS.desktopAPI.event.CallStart.registerListener(function(e) {
            onCallStart(e);
        })
        LIVEOPS.desktopAPI.event.CallEnd.registerListener(function(e) {
            onCallEnd(e);
        })
        LIVEOPS.desktopAPI.event.AgentOnline.registerListener(function(e) {
           onAgentOnline(e);
        })
        LIVEOPS.desktopAPI.event.AgentOffline.registerListener(function(e) {
            onAgentOffline(e);
        })


        sforce.interaction.cti.onClickToDial(function(e){
            onClickToDial(e);
        });

    }

    sfdcMediator.init =
    function init() {
        log("Register event listeners");
        registerEventListener();
    }

    if(!LIVEOPS){
        throw new Error("LIVEOPS agent needs to be present to use this API.");
    } else {
        LIVEOPS.sfdcMediator = sfdcMediator;
    }


})(LIVEOPS, sforce, jQuery);